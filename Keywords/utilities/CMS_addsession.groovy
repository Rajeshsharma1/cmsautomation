package utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class CMS_Customs {

	@Keyword
	public static void login() {

		String url = 'http://192.168.1.17:3003/'

		WebUI.navigateToUrl(url)

		//WebUI.openBrowser('https://test-admin.thewellnesscorner.com/login')
		WebUI.maximizeWindow()

		WebUI.setText(findTestObject('login/Page_TWC - Content Management System/input_Email address_formBasicEmail'), GlobalVariable.username)

		WebUI.setText(findTestObject('login/Page_TWC - Content Management System/input_Password_formBasicPassword'), GlobalVariable.password)

		WebUI.click(findTestObject('login/Page_TWC - Content Management System/button_Submit'))

		if (WebUI.verifyElementPresent(findTestObject('login/Page_TWC - Content Management System/div_Invalid password'), 10, FailureHandling.OPTIONAL)) {
			WebUI.comment('User is not logged in due to invalid credentials.')

			WebUI.closeBrowser()

			assert false : 'User is not logged in due to invalid credentials.'

		}
	}
}
