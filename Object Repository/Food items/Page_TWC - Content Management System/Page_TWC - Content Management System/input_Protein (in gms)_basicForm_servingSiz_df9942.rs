<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Protein (in gms)_basicForm_servingSiz_df9942</name>
   <tag></tag>
   <elementGuidId>cb38bcaa-0db3-4c10-b35e-9be4daa8f2b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basicForm_servingSizes_1_protein']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#basicForm_servingSizes_1_protein</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>70638e26-d2fb-4d2d-9f43-5a34f9e43e53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basicForm_servingSizes_1_protein</value>
      <webElementGuid>0d7650a5-3d20-4c80-90e1-e2ef19372725</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input ant-input-lg</value>
      <webElementGuid>b8080b1e-c883-4e84-be28-703c115cb1fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>bff6043c-0e28-4c83-aacb-07ecf45f274c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm_servingSizes_1_protein&quot;)</value>
      <webElementGuid>c6edd299-0504-4787-bd19-a940b0e59bea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basicForm_servingSizes_1_protein']</value>
      <webElementGuid>a7731e4c-9a5f-4be8-a37e-8339286ac030</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basicForm']/div[5]/div[2]/div/div[4]/div[2]/div/div[2]/div/div/input</value>
      <webElementGuid>caacd074-8d13-4da1-a242-70cfc095ec37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[4]/div[2]/div/div[2]/div/div/input</value>
      <webElementGuid>e6697051-41fa-4466-aded-4d454d4c47f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'basicForm_servingSizes_1_protein' and @type = 'text']</value>
      <webElementGuid>987fcac7-d305-46b2-8b8a-95552b7b480d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
