<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__basicForm_servingSizes_1_standardMeasure_quantity</name>
   <tag></tag>
   <elementGuidId>894a9220-9857-4d6b-b88d-5f16753f5a26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basicForm_servingSizes_1_standardMeasure_quantity']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#basicForm_servingSizes_1_standardMeasure_quantity</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c248e98e-466b-4da3-a174-332c0c946465</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basicForm_servingSizes_1_standardMeasure_quantity</value>
      <webElementGuid>9abf42d6-985f-4c03-8ad3-8f3fbfee7f09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input ant-input-lg</value>
      <webElementGuid>c42d6c4b-8f02-474a-ac12-4c865802c02a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>aa394c1e-5262-4aa1-8c1d-a6e35b360c68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm_servingSizes_1_standardMeasure_quantity&quot;)</value>
      <webElementGuid>be2c9896-0765-4a7a-b8f0-3a4c881a58c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basicForm_servingSizes_1_standardMeasure_quantity']</value>
      <webElementGuid>2ce06aff-7f91-4441-aad6-7bfb1e92cde3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basicForm']/div[5]/div[2]/div/div[2]/div[2]/div/div/div/div/input</value>
      <webElementGuid>adc6484a-d6c2-4025-a06d-3f320ec5e8df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div/div/div/div/input</value>
      <webElementGuid>28c58d50-a2e9-42cf-99b5-7b647238b607</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'basicForm_servingSizes_1_standardMeasure_quantity' and @type = 'text']</value>
      <webElementGuid>70eb2cc5-2c93-4455-99a8-edf30cedd878</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
