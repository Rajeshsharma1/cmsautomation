<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Sodium (in mg)_basicForm_servingSizes_7ac3d5</name>
   <tag></tag>
   <elementGuidId>2fde18f5-3bc7-4a96-99e6-8717c9da00a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basicForm_servingSizes_1_sodium']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#basicForm_servingSizes_1_sodium</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5a566ecb-5e59-44b8-8ccb-b39e089d2942</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basicForm_servingSizes_1_sodium</value>
      <webElementGuid>d618f6de-babf-4051-a246-ffe31302a798</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input ant-input-lg</value>
      <webElementGuid>b3c57515-1f01-463f-baac-c87f21afc84d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>13890161-d194-46eb-a4ee-bef93734a5d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm_servingSizes_1_sodium&quot;)</value>
      <webElementGuid>386e841d-c823-4d75-996d-1fa6480b18a4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basicForm_servingSizes_1_sodium']</value>
      <webElementGuid>cfbbb704-b07a-4f1b-8929-f1af5178213b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basicForm']/div[5]/div[2]/div/div[5]/div/div/div[2]/div/div/input</value>
      <webElementGuid>eddc80f5-a059-489c-9880-2fd9ce82310a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[5]/div/div/div[2]/div/div/input</value>
      <webElementGuid>c715bc22-45bd-48bc-9141-bc79b0b3a762</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'basicForm_servingSizes_1_sodium' and @type = 'text']</value>
      <webElementGuid>0147f435-1814-4de4-8156-d0945e0c1e06</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
