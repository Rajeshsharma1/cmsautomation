<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Click to Upload</name>
   <tag></tag>
   <elementGuidId>6734234d-0202-44b5-99ba-7de202b96972</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='basicForm']/div[3]/div[2]/div/div/span/div/span/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.ant-upload > div > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8e696863-3e7c-4f64-9954-ccf7666c9cd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Click to Upload</value>
      <webElementGuid>ce95b083-29ab-4308-a21b-c2ea4434e3db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm&quot;)/div[@class=&quot;ant-row ant-form-item fullWidthUpload&quot;]/div[@class=&quot;ant-col ant-col-24 ant-form-item-control&quot;]/div[@class=&quot;ant-form-item-control-input&quot;]/div[@class=&quot;ant-form-item-control-input-content&quot;]/span[@class=&quot;ant-upload-picture-card-wrapper avatar-uploader hover_color&quot;]/div[@class=&quot;ant-upload ant-upload-select ant-upload-select-picture-card&quot;]/span[@class=&quot;ant-upload&quot;]/div[1]/div[1]</value>
      <webElementGuid>f876e759-2e3c-4df7-88bd-032372252c38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2fa57397-7402-4e73-af33-fbcd5c208ee9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Click to Upload</value>
      <webElementGuid>fda94c71-6d48-472c-812d-d1342067a176</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm&quot;)/div[@class=&quot;ant-row ant-form-item fullWidthUpload&quot;]/div[@class=&quot;ant-col ant-col-24 ant-form-item-control&quot;]/div[@class=&quot;ant-form-item-control-input&quot;]/div[@class=&quot;ant-form-item-control-input-content&quot;]/span[@class=&quot;ant-upload-picture-card-wrapper avatar-uploader hover_color&quot;]/div[@class=&quot;ant-upload ant-upload-select ant-upload-select-picture-card&quot;]/span[@class=&quot;ant-upload&quot;]/div[1]/div[1]</value>
      <webElementGuid>ffadb59b-981e-4cb8-a769-fa5b5fe6fed6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>af17536d-1817-4791-b909-790f2bf6bafb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Click to Upload</value>
      <webElementGuid>9379312c-020f-43cb-91d6-ec78914b2207</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm&quot;)/div[@class=&quot;ant-row ant-form-item fullWidthUpload&quot;]/div[@class=&quot;ant-col ant-col-24 ant-form-item-control&quot;]/div[@class=&quot;ant-form-item-control-input&quot;]/div[@class=&quot;ant-form-item-control-input-content&quot;]/span[@class=&quot;ant-upload-picture-card-wrapper avatar-uploader hover_color&quot;]/div[@class=&quot;ant-upload ant-upload-select ant-upload-select-picture-card&quot;]/span[@class=&quot;ant-upload&quot;]/div[1]/div[1]</value>
      <webElementGuid>1856174a-4b56-4c92-a1a1-f8242090ee28</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basicForm']/div[3]/div[2]/div/div/span/div/span/div/div</value>
      <webElementGuid>225d05a9-3159-426f-8e73-32c20743080c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Image'])[1]/following::div[6]</value>
      <webElementGuid>9291ed80-d9cc-4e20-b6c4-cf662a0c7a7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Food Type'])[2]/following::div[8]</value>
      <webElementGuid>45c71ad1-6fee-43f7-9750-5f0c408a688a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alias'])[1]/preceding::div[1]</value>
      <webElementGuid>51f10330-2b0d-4934-a524-0f22fdef50e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please enter alias'])[1]/preceding::div[5]</value>
      <webElementGuid>d3c598cd-d798-4ec2-8d07-2640da78b201</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Click to Upload']/parent::*</value>
      <webElementGuid>2a6f2720-35e7-4f55-9e53-7b243dc9129d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/div</value>
      <webElementGuid>22395e07-0e35-4277-bbf2-3723d8611c41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Click to Upload' or . = ' Click to Upload')]</value>
      <webElementGuid>94c2c99e-e4bb-4fff-91d4-04b7d44608c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
