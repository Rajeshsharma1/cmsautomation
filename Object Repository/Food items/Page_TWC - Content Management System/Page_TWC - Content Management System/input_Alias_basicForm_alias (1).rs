<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Alias_basicForm_alias (1)</name>
   <tag></tag>
   <elementGuidId>0b8299af-d584-4114-bce8-136e90366e9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;basicForm&quot;]/div[4]/div[1]/div/div[2]/div[1]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#basicForm_alias</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d6b6c592-a974-4047-8802-8768abe2e450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>search</value>
      <webElementGuid>5c0f54a7-93a1-4c38-98aa-ab16cf832a8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basicForm_alias</value>
      <webElementGuid>40972c14-649b-4a55-bc38-dbce51248e91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>dd28bd0b-98c4-4665-ad1f-3d2b2f29a842</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-selection-search-input</value>
      <webElementGuid>af4407e6-62cf-4813-a45f-8f6b69dddd5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>combobox</value>
      <webElementGuid>6dec8efb-c960-404b-9446-a8377de1ed93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>listbox</value>
      <webElementGuid>afb17a0f-0b0a-4bf8-92bb-0d1f8abc532b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>basicForm_alias_list</value>
      <webElementGuid>f7a54cc8-e665-494c-bcd3-da966bede292</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>32f82625-2e7b-4dda-9623-7133dcddbcb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>basicForm_alias_list</value>
      <webElementGuid>d9de0f84-4bde-4607-a4ea-4e755527b81d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-activedescendant</name>
      <type>Main</type>
      <value>basicForm_alias_list_0</value>
      <webElementGuid>b2a37ef7-2a74-4c03-a140-10d7869e5e77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>5413b4da-f51e-4a73-975c-9d3ea36681af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f1a41e57-f4d4-4495-8cfc-f01c1d3093da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basicForm_alias&quot;)</value>
      <webElementGuid>db2d2ef7-6f5f-46ca-9c77-11ec17d106f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basicForm_alias']</value>
      <webElementGuid>254ed88f-6b17-403e-b19a-58f6bbb3b57d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basicForm']/div[4]/div/div/div[2]/div/div/div/div/div/div/div/input</value>
      <webElementGuid>68997519-1a42-4ddf-a9b0-7ee1dcfd4a37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div/input</value>
      <webElementGuid>34b550f5-1872-4df1-a5f3-5c2ccd785692</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'search' and @id = 'basicForm_alias']</value>
      <webElementGuid>7c2f6dd5-2809-43b2-9ae3-9620d98c2d5c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
