<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Workout PlansGuided ProgramsDiet Pl_ae93ff</name>
   <tag></tag>
   <elementGuidId>ff6f8815-152a-48cd-aa73-aec77a7fcb85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/section</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>section.ant-layout.ant-layout-has-sider</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>6c057eb1-fb59-43c3-938b-15e940270444</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-layout ant-layout-has-sider</value>
      <webElementGuid>ea24bb12-6b79-4774-b72d-d3066beb8320</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Workout PlansGuided ProgramsDiet PlansRecipesMeditationsAssessmentsTagsFoodFormsUsersActivity LogLogoutCreate Workout PlanAll Workout PlansWorkout PlansActionstestworkout570Image TestingTest plan nametest123New Workout Deeps Demotest again again againVideo Workout PlanNew Workout Plan TwoNew Workout 20Augthe wellness cornerRows per page:10 rows 1-10 of 181-10 of 18</value>
      <webElementGuid>f558c8f1-6aa5-43e7-a713-717477de92a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/section[@class=&quot;ant-layout ant-layout-has-sider&quot;]</value>
      <webElementGuid>894e6f08-38ed-4cb1-9157-b4a91c705b12</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/section</value>
      <webElementGuid>0e0af248-fa9c-4f87-bd5c-4bb91ee533c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>cc545587-5869-4f49-bc3d-cb2d30d01fd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = 'Workout PlansGuided ProgramsDiet PlansRecipesMeditationsAssessmentsTagsFoodFormsUsersActivity LogLogoutCreate Workout PlanAll Workout PlansWorkout PlansActionstestworkout570Image TestingTest plan nametest123New Workout Deeps Demotest again again againVideo Workout PlanNew Workout Plan TwoNew Workout 20Augthe wellness cornerRows per page:10 rows 1-10 of 181-10 of 18' or . = 'Workout PlansGuided ProgramsDiet PlansRecipesMeditationsAssessmentsTagsFoodFormsUsersActivity LogLogoutCreate Workout PlanAll Workout PlansWorkout PlansActionstestworkout570Image TestingTest plan nametest123New Workout Deeps Demotest again again againVideo Workout PlanNew Workout Plan TwoNew Workout 20Augthe wellness cornerRows per page:10 rows 1-10 of 181-10 of 18')]</value>
      <webElementGuid>92c30281-830d-4e7d-804f-6ecb8ee56a90</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
