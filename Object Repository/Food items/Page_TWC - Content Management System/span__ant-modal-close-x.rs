<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span__ant-modal-close-x</name>
   <tag></tag>
   <elementGuidId>28c0f963-8413-422f-b6c0-944064287b11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='•••'])[1]/following::span[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.ant-modal-close-x</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>720b5aef-1f79-415c-baa4-85bc3d720f91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-modal-close-x</value>
      <webElementGuid>21832398-05dc-4d62-9358-bf6adf24ad6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ant-scrolling-effect&quot;]/div[5]/div[@class=&quot;ant-modal-root&quot;]/div[@class=&quot;ant-modal-wrap&quot;]/div[@class=&quot;ant-modal&quot;]/div[@class=&quot;ant-modal-content&quot;]/button[@class=&quot;ant-modal-close&quot;]/span[@class=&quot;ant-modal-close-x&quot;]</value>
      <webElementGuid>051eef74-48d9-44db-982c-ab3c2e0b6cdc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='•••'])[1]/following::span[6]</value>
      <webElementGuid>5a5f0fb6-5237-4eb8-8987-dacdf13f7a49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Vegetarian'])[9]/following::span[12]</value>
      <webElementGuid>43a8d923-a04f-49fe-adeb-3e7945060185</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Food Detail'])[1]/preceding::span[2]</value>
      <webElementGuid>4ce12f66-3c1c-484d-a63f-9dc84f46c947</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Food123'])[2]/preceding::span[2]</value>
      <webElementGuid>7349fcdc-8c1e-477a-9503-832b4896e185</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>255d9495-0b96-4a0d-8b22-b3d0282477c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
