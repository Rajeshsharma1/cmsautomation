<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email address_formBasicEmail</name>
   <tag></tag>
   <elementGuidId>b68b0fc6-668c-4c92-a8f1-f490819fbdb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=&quot;email&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#formBasicEmail</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>231b8fa4-bc2c-4fa9-ab84-79b85f2302f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter email</value>
      <webElementGuid>d4110bee-c680-4dbc-85ee-edf21fe656bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>14f03332-7b6d-49d9-812e-3a372f3acad5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>formBasicEmail</value>
      <webElementGuid>b5e17d16-0520-4258-adad-093865b127b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>a88b0a2b-1322-4895-9207-7458e41d83b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;formBasicEmail&quot;)</value>
      <webElementGuid>18686537-123d-4240-9f8f-6016044a3e93</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='formBasicEmail']</value>
      <webElementGuid>b7c8a8f8-5381-4460-9692-a4972dddc471</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div/form/div/input</value>
      <webElementGuid>7089451e-3424-4246-a36f-f044dc3ee67d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>3d5bb2a9-0cae-411c-a043-ecb859a718ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Enter email' and @type = 'email' and @id = 'formBasicEmail']</value>
      <webElementGuid>d5e94de5-a8ba-46ec-887d-7208eb1c2698</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
