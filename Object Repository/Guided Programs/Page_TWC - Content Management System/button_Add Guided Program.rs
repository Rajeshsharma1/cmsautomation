<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add Guided Program</name>
   <tag></tag>
   <elementGuidId>4efa8042-ceda-4b6e-b81c-34f51c681966</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='btn btn-info']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-info</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a97858a5-bd1c-4522-83ca-e0b833eca4c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-info</value>
      <webElementGuid>3a65ed91-5128-4c2a-82d5-239310a20554</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Guided Program</value>
      <webElementGuid>8152bc17-218b-4f5c-b002-7e0323a2476b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;_1v7ay&quot;]/div[@class=&quot;_2tA-A _1z61Y&quot;]/div[@class=&quot;_2NTVU&quot;]/div[@class=&quot;table-container&quot;]/div[@class=&quot;toolbar&quot;]/button[@class=&quot;btn btn-info&quot;]</value>
      <webElementGuid>e0bbd029-4253-4859-85aa-dfa833b4e0ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/button</value>
      <webElementGuid>d7a891e5-593f-4115-96e9-84d7fc8546a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Activity Log'])[1]/following::button[1]</value>
      <webElementGuid>d069a98f-4b3e-4f77-b015-447912d69625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Users'])[1]/following::button[1]</value>
      <webElementGuid>17e0541f-da9a-4ab8-bc8c-16e36308201d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Guided Programs'])[1]/preceding::button[1]</value>
      <webElementGuid>df1e3996-cc20-46e7-80c6-4e206e24e71c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Guided Programs'])[2]/preceding::button[2]</value>
      <webElementGuid>de49e9e8-57d5-4eaf-9c19-a0d28e5d8e24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Guided Program']/parent::*</value>
      <webElementGuid>0ae7de58-b5d4-4426-83db-a92d562d888e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>43d20276-1de0-4f00-92b8-1fce3ed09ed6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add Guided Program' or . = 'Add Guided Program')]</value>
      <webElementGuid>3694315a-d0ce-4568-8909-7b42749fcf3f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
