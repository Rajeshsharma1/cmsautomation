<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create Food Group</name>
   <tag></tag>
   <elementGuidId>81e8c258-7038-4ae2-8596-e2603811f3af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-space-item > button.ant-btn.ant-btn-primary.ant-btn-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>927ee186-40d0-4d2f-af53-43197b6a17ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2cb10262-f67f-4ea6-95e8-e8687532ab9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn ant-btn-primary ant-btn-lg</value>
      <webElementGuid>4c6efa7b-33fd-4235-8d36-13481bb0df3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Food Group</value>
      <webElementGuid>76c911a8-aac3-4ccc-9f8a-91f952cb516f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/section[@class=&quot;ant-layout ant-layout-has-sider&quot;]/section[@class=&quot;ant-layout&quot;]/div[2]/div[@class=&quot;ant-page-header tagsPageHeader antpageLayout ant-page-header-ghost&quot;]/div[@class=&quot;ant-page-header-heading&quot;]/span[@class=&quot;ant-page-header-heading-extra&quot;]/div[@class=&quot;ant-space ant-space-horizontal ant-space-align-center&quot;]/div[@class=&quot;ant-space-item&quot;]/button[@class=&quot;ant-btn ant-btn-primary ant-btn-lg&quot;]</value>
      <webElementGuid>db2c0253-61c8-4777-a505-549af3a7570d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[3]</value>
      <webElementGuid>6e38f5db-2c89-4630-b647-9ab967eb2dea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/section/section/div[2]/div/div/span/div/div[2]/button</value>
      <webElementGuid>8daafa8a-626a-476b-b929-9c6ec3a5d80f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Food Groups'])[1]/following::button[2]</value>
      <webElementGuid>d446c72d-f7a2-4b4b-9dfd-d8c31b30b857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::button[2]</value>
      <webElementGuid>ee9ad886-2ae6-403f-b8d6-6e3fcf383687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='#'])[1]/preceding::button[1]</value>
      <webElementGuid>5208c3c3-7ce8-4a5a-afc5-f4d9a555387a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>c4e687a5-c2d1-48f1-b858-327baca30cf4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Create Food Group' or . = 'Create Food Group')]</value>
      <webElementGuid>4efe12bb-ce90-4acb-9836-95974ba014e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
