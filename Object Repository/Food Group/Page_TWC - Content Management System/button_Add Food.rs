<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add Food</name>
   <tag></tag>
   <elementGuidId>a1e459f0-e7d1-49b3-a52d-d96f21c04e8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.ant-btn.ant-btn-primary.ant-btn-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>036bb3d4-42a9-4d31-af56-26dfc2504a53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>03ea8003-a6f6-4121-8b22-1aefd1f6dd89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn ant-btn-primary ant-btn-lg</value>
      <webElementGuid>5baec5fd-a585-41d8-88af-a76adfcb5497</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Food</value>
      <webElementGuid>ce382ecb-ac52-4ee4-bf9e-28ca86a21b42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/section[@class=&quot;ant-layout ant-layout-has-sider&quot;]/section[@class=&quot;ant-layout&quot;]/div[2]/div[@class=&quot;ant-page-header ant-page-header-ghost&quot;]/div[@class=&quot;ant-page-header-heading&quot;]/span[@class=&quot;ant-page-header-heading-extra&quot;]/div[@class=&quot;ant-space ant-space-horizontal ant-space-align-center&quot;]/div[@class=&quot;ant-space-item&quot;]/button[@class=&quot;ant-btn ant-btn-primary ant-btn-lg&quot;]</value>
      <webElementGuid>b1eee355-2341-433d-acb3-399ad3ab9a7b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[2]</value>
      <webElementGuid>6f1fc3b7-8121-4efb-875f-ff4c24509d11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/section/section/div[2]/div/div/span/div/div/button</value>
      <webElementGuid>cb482f15-3bad-4d98-b58f-be5e0aa4822f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(Breakfast)'])[1]/following::button[1]</value>
      <webElementGuid>302a1988-3347-4311-bc8b-df90be1c8c15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test 1234'])[1]/following::button[1]</value>
      <webElementGuid>7faa687f-ab34-4b81-b914-d11fcee66af8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No Data'])[1]/preceding::button[1]</value>
      <webElementGuid>0c5e6789-7afe-4135-9fc5-132ced4ad541</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>51977c1d-4d39-459b-8977-61e6f164a783</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Add Food' or . = 'Add Food')]</value>
      <webElementGuid>97e029c5-99c4-421b-96f2-8aca2b2028c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
