<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Food Group Name_basic_name</name>
   <tag></tag>
   <elementGuidId>a71ae51a-4167-4d0c-8b13-98537efa1ec0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#basic_name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basic_name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0e071e92-3b3d-42b6-aa08-28124c0fc3b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>200</value>
      <webElementGuid>1aff2e10-6719-4635-a15c-41eedd1b4a25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter Food Group Name</value>
      <webElementGuid>f8093efd-c138-4932-b9f7-5289a5c18a36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>basic_name</value>
      <webElementGuid>36399d5f-9282-41c0-9b7f-b8e7a94a5fa3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input</value>
      <webElementGuid>34735800-19d1-42e6-bd9e-711a552abf28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>335444ca-4f06-494a-8618-672fea6d3811</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basic_name&quot;)</value>
      <webElementGuid>6bddb8b1-78a3-43a9-a9a6-ff263c0db866</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='basic_name']</value>
      <webElementGuid>62e84053-db12-439f-8fc2-bd4849419d61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basic']/div/div[2]/div/div/input</value>
      <webElementGuid>7b04606e-d50a-4ad1-9d4c-89ca64faea3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>449bdfae-1399-4024-9f76-e241d2eebfca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Enter Food Group Name' and @id = 'basic_name' and @type = 'text']</value>
      <webElementGuid>fb589d4f-2e10-4396-a8c7-d081aad7a1c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
