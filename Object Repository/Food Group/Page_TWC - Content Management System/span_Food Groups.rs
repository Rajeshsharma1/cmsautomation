<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Food Groups</name>
   <tag></tag>
   <elementGuidId>d62573ec-7229-4b18-9007-9f2520792ef7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-item.ant-menu-item-active > span.ant-menu-title-content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='rc-menu-uuid-19748-1-tmp_key-7-popup']/li[2]/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>375fc455-61f2-4a98-879f-643a8d2a1784</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-menu-title-content</value>
      <webElementGuid>7e73561c-3b65-4a6c-9dc0-09c25dc0e774</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Food Groups</value>
      <webElementGuid>3283f093-fd01-4cb4-8328-4e2f48ec8db2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rc-menu-uuid-19748-1-tmp_key-7-popup&quot;)/li[@class=&quot;ant-menu-item ant-menu-item-active&quot;]/span[@class=&quot;ant-menu-title-content&quot;]</value>
      <webElementGuid>39aa788f-98b7-4951-9121-a478f24e017a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='rc-menu-uuid-19748-1-tmp_key-7-popup']/li[2]/span[2]</value>
      <webElementGuid>1b4408ef-1a24-4ce4-851b-48ccc83fad59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Food Items'])[1]/following::span[2]</value>
      <webElementGuid>ff892b2c-1e86-44e6-b40c-e03608446dbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Food'])[1]/following::span[4]</value>
      <webElementGuid>13a455de-286b-4182-95a8-8bf771b3d66d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forms'])[1]/preceding::span[2]</value>
      <webElementGuid>5187c5c6-6e3f-4cb2-ab79-e29088310e44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Users'])[1]/preceding::span[4]</value>
      <webElementGuid>2dc9dca5-a5f7-4731-894a-7296e27dee1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Food Groups']/parent::*</value>
      <webElementGuid>da95a117-eb72-4eb4-b272-2f1552dda0da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[8]/ul/li[2]/span[2]</value>
      <webElementGuid>6d24274b-5c2d-42d3-a9b6-2d1d07bdf988</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Food Groups' or . = 'Food Groups')]</value>
      <webElementGuid>6a544f77-3805-4e17-8b57-06924fcc21d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
