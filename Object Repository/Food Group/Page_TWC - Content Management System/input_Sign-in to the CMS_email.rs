<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Sign-in to the CMS_email</name>
   <tag></tag>
   <elementGuidId>a09d7b46-5d4d-4fa5-9f4d-37a2bf1e9ffb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2faeff7f-2793-4fb3-8801-07e901ff2665</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Email ID</value>
      <webElementGuid>818496ad-010e-4ee3-8fc4-b6763a4953d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>5523cde0-8503-4939-a5ca-71552c04cdac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input ant-input-lg</value>
      <webElementGuid>2ff417f9-277d-49fd-8c20-d32a731b266c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>80f2f171-91b9-482e-8915-76562b30746a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;email&quot;)</value>
      <webElementGuid>ac3baccd-dca6-485e-9659-16ed0a31e8f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='email']</value>
      <webElementGuid>472cd94b-0867-473b-95e8-1cc955293e13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div/form/div/div/div/div/input</value>
      <webElementGuid>02d9eb2b-42f0-49d4-93f0-e7a87ed724d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>6a1ec534-27ec-45e8-a6c7-09649b22a594</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Email ID' and @id = 'email' and @type = 'text']</value>
      <webElementGuid>8fb1a9bd-5451-4e5c-812d-afb43a9352a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
