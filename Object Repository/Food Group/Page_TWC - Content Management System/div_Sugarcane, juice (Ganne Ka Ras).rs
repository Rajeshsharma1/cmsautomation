<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sugarcane, juice (Ganne Ka Ras)</name>
   <tag></tag>
   <elementGuidId>8f91d741-0964-4579-a49a-d92c4f1aeea5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-select-item.ant-select-item-option.ant-select-item-option-active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Finger Millet Juice (Nachni Udha)'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>332bb326-7320-4025-94f0-62b58a764762</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e7f45847-24de-48fa-9184-6577c65aa047</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-item ant-select-item-option ant-select-item-option-active</value>
      <webElementGuid>eb5d145f-7e81-4d77-9e20-3192c69d01b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Sugarcane, juice (Ganne Ka Ras)</value>
      <webElementGuid>58488263-f62f-417d-be50-8bbb3d9357e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sugarcane, juice (Ganne Ka Ras)</value>
      <webElementGuid>699df0b4-e8a5-45d6-b870-dc0a82db2186</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ant-scrolling-effect&quot;]/div[5]/div[1]/div[@class=&quot;ant-select-dropdown ant-select-dropdown-placement-bottomLeft&quot;]/div[1]/div[@class=&quot;rc-virtual-list&quot;]/div[@class=&quot;rc-virtual-list-holder&quot;]/div[1]/div[@class=&quot;rc-virtual-list-holder-inner&quot;]/div[@class=&quot;ant-select-item ant-select-item-option ant-select-item-option-active&quot;]</value>
      <webElementGuid>d0d123ca-765f-4363-acfb-59d12341763d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Finger Millet Juice (Nachni Udha)'])[1]/following::div[1]</value>
      <webElementGuid>557b46d4-17e0-4550-82cf-ecedfdac75c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/following::div[18]</value>
      <webElementGuid>609f638c-efb4-4565-b23a-0e5f9a939fc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Real Wellness Jamun Juice'])[1]/preceding::div[2]</value>
      <webElementGuid>bbb95792-8b68-4ab1-b6ed-84c19dd2fe2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]</value>
      <webElementGuid>6f682043-556b-4b50-b0e2-5465724d0d2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'Sugarcane, juice (Ganne Ka Ras)' and (text() = 'Sugarcane, juice (Ganne Ka Ras)' or . = 'Sugarcane, juice (Ganne Ka Ras)')]</value>
      <webElementGuid>3a8cf61a-a47e-4302-9074-c1593d34ae89</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
