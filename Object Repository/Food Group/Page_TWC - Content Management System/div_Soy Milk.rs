<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Soy Milk</name>
   <tag></tag>
   <elementGuidId>c57bc1f6-1915-491a-81a1-9c968a07af2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ant-select-item.ant-select-item-option.ant-select-item-option-active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Coconut Milk Strawberry Smoothie Keto'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>570128f9-7c97-4db2-bb18-fe72b55764a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e32d77d6-d99c-436f-8bff-e36976887ceb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-item ant-select-item-option ant-select-item-option-active</value>
      <webElementGuid>9fd6e4d6-b8dd-43b6-881c-4ce562528d66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Soy Milk</value>
      <webElementGuid>383d0c4b-b3ed-4f59-8220-ce7d80e0caa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Soy Milk</value>
      <webElementGuid>1ac48364-3788-449d-87d4-94dbd55cd17a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ant-scrolling-effect&quot;]/div[4]/div[1]/div[@class=&quot;ant-select-dropdown ant-select-dropdown-placement-bottomLeft&quot;]/div[1]/div[@class=&quot;rc-virtual-list&quot;]/div[@class=&quot;rc-virtual-list-holder&quot;]/div[1]/div[@class=&quot;rc-virtual-list-holder-inner&quot;]/div[@class=&quot;ant-select-item ant-select-item-option ant-select-item-option-active&quot;]</value>
      <webElementGuid>c973cf52-6eef-4a35-817b-44144e67c7b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coconut Milk Strawberry Smoothie Keto'])[1]/following::div[1]</value>
      <webElementGuid>a8b78a87-0923-4c20-a7cf-ca20a812b80a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/following::div[16]</value>
      <webElementGuid>f4321ff8-8f4c-472f-8312-4f94ee6f0048</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nasi Lemak, packet(fragrent rice with coconut milk,eggs and anchovies)'])[1]/preceding::div[2]</value>
      <webElementGuid>aed46551-57dd-413e-b053-6567c2eb0e55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]</value>
      <webElementGuid>b4c1fcd6-bf3f-491b-b271-55f26bf05513</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'Soy Milk' and (text() = 'Soy Milk' or . = 'Soy Milk')]</value>
      <webElementGuid>dbd07499-6d01-4b4c-8f07-28bc13c2bc62</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
