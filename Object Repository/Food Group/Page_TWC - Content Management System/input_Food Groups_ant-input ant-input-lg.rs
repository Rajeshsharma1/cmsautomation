<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Food Groups_ant-input ant-input-lg</name>
   <tag></tag>
   <elementGuidId>9ab9f1bd-a33d-4234-b05c-637e708eb06b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.ant-input.ant-input-lg</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>6260eef3-5664-4397-92ba-beae59e181d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search by Food Group Name</value>
      <webElementGuid>5f49ff79-e586-440d-a83b-fe424ce2fcc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-input ant-input-lg</value>
      <webElementGuid>85bd7f65-6d16-4659-bdad-3e1d490b03fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d4b5049f-2955-46c9-89c2-f6a42e773d2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/section[@class=&quot;ant-layout ant-layout-has-sider&quot;]/section[@class=&quot;ant-layout&quot;]/div[2]/div[@class=&quot;ant-page-header tagsPageHeader antpageLayout ant-page-header-ghost&quot;]/div[@class=&quot;ant-page-header-heading&quot;]/span[@class=&quot;ant-page-header-heading-extra&quot;]/div[@class=&quot;ant-space ant-space-horizontal ant-space-align-center&quot;]/div[@class=&quot;ant-space-item&quot;]/span[@class=&quot;ant-input-group-wrapper ant-input-search ant-input-search-large ant-input-search-with-button ant-input-group-wrapper-lg&quot;]/span[@class=&quot;ant-input-wrapper ant-input-group&quot;]/span[@class=&quot;ant-input-affix-wrapper ant-input-affix-wrapper-focused ant-input-affix-wrapper-lg&quot;]/input[@class=&quot;ant-input ant-input-lg&quot;]</value>
      <webElementGuid>dd1d4753-3ea6-4db1-98c6-3514e6088397</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='']</value>
      <webElementGuid>3e1f938b-bab2-41d3-ba6b-5b878d017d46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/section/section/div[2]/div/div/span/div/div/span/span/span/input</value>
      <webElementGuid>80ccfb97-e832-4607-9f48-36f3d93b2ad0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>a3f6c6b2-0ad6-4af4-94d3-dd1a2a8fb458</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Search by Food Group Name' and @type = 'text']</value>
      <webElementGuid>484b3364-7f94-4743-b67c-231994d96a90</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
