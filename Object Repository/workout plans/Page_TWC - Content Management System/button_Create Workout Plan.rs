<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create Workout Plan</name>
   <tag></tag>
   <elementGuidId>11f8f56b-3ff3-4c3f-889d-a6d2644033d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='btn btn-info']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-info</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fd187aa6-2ee7-457b-a7b3-2c5af7aa2f87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-info</value>
      <webElementGuid>64cb3609-d001-48e5-ab9f-04c97c9ecfa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Workout Plan</value>
      <webElementGuid>ab292aa5-f3fd-4c96-9325-7b37810dec2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;_1v7ay&quot;]/div[@class=&quot;_2tA-A _1z61Y&quot;]/div[@class=&quot;_2NTVU&quot;]/div[@class=&quot;table-container&quot;]/div[@class=&quot;toolbar&quot;]/button[@class=&quot;btn btn-info&quot;]</value>
      <webElementGuid>d3c004dc-1073-44e0-b5a8-c5d440a3e96a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/button</value>
      <webElementGuid>224502a7-8231-4e97-92f3-57292addf002</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Activity Log'])[1]/following::button[1]</value>
      <webElementGuid>510e1248-4271-46d8-be46-9e44d1e70c47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Users'])[1]/following::button[1]</value>
      <webElementGuid>a8053320-21f1-4db3-a8e1-c0180a8c3023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Workout Plans'])[1]/preceding::button[1]</value>
      <webElementGuid>5c9b74b0-4911-4b72-86b8-887626ecf610</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Workout Plans'])[2]/preceding::button[2]</value>
      <webElementGuid>014f7b15-9dd4-4c79-acf4-53f7e3dbf1f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create Workout Plan']/parent::*</value>
      <webElementGuid>99ad5c96-e019-4b68-8e6b-dbf4e3556225</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>df91824d-a686-4e91-9d32-c740581c5d9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Create Workout Plan' or . = 'Create Workout Plan')]</value>
      <webElementGuid>c1647275-524f-416a-b0a8-c0cc6d71baee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
