<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_WELLNESS-TV VIDEO</name>
   <tag></tag>
   <elementGuidId>72946647-b015-4d3f-9b47-99ec5cce32ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='guided-programs'])[1]/following::div[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-select-item-option-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7e8e3671-f405-4643-b7e3-9df44f8c7cd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-item-option-content</value>
      <webElementGuid>7bbf965a-f40c-4881-b912-756fa97da5a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>WELLNESS-TV VIDEO</value>
      <webElementGuid>c08ff8ae-0592-4677-908c-ec7d8b27fd09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ant-scrolling-effect&quot;]/div[3]/div[1]/div[@class=&quot;ant-select-dropdown ant-select-dropdown-placement-bottomLeft&quot;]/div[1]/div[@class=&quot;rc-virtual-list&quot;]/div[@class=&quot;rc-virtual-list-holder&quot;]/div[1]/div[@class=&quot;rc-virtual-list-holder-inner&quot;]/div[@class=&quot;ant-select-item ant-select-item-option ant-select-item-option-active&quot;]/div[@class=&quot;ant-select-item-option-content&quot;]</value>
      <webElementGuid>3c60d2da-6d83-4900-9ddc-62158f5e2661</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='guided-programs'])[1]/following::div[6]</value>
      <webElementGuid>dc275b8a-794b-49ae-ba78-a1a1e87708d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='wellness-tv'])[1]/following::div[7]</value>
      <webElementGuid>4768b022-c5d7-4665-af5d-46e08e36b82e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GUIDED PROGRAMS'])[1]/preceding::div[1]</value>
      <webElementGuid>dcb9e848-6e17-4d8d-b9d1-c3e1db3acfc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RECIPE'])[1]/preceding::div[3]</value>
      <webElementGuid>c7150608-852c-4318-95a8-c03e64027e33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='WELLNESS-TV VIDEO']/parent::*</value>
      <webElementGuid>5955a736-8722-463d-896c-b0a995c54894</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div/div/div</value>
      <webElementGuid>993800a3-5a8e-480f-9433-22e818590db8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'WELLNESS-TV VIDEO' or . = 'WELLNESS-TV VIDEO')]</value>
      <webElementGuid>92544342-f48c-4722-a0ba-c3ea740100e4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
