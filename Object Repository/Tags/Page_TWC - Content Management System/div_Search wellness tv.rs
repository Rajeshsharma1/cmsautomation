<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Search wellness tv</name>
   <tag></tag>
   <elementGuidId>62625c24-6952-4dc9-ba44-4e1e7d27c993</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='guided-programs'])[1]/following::div[6]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-select.ant-select-in-form-item.ant-select-auto-complete.ant-select-single.ant-select-open.ant-select-show-search > div.ant-select-selector</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>202d0482-1b43-4057-b4d1-78d3411358c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-selector</value>
      <webElementGuid>0228b342-f570-47b5-8f37-54d56774c4b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Search wellness tv</value>
      <webElementGuid>f6c2b0e8-d2ad-4a84-8253-ecfc84fd38f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basic&quot;)/div[@class=&quot;ant-row ant-form-item&quot;]/div[@class=&quot;ant-col ant-col-18 ant-form-item-control&quot;]/div[@class=&quot;ant-form-item-control-input&quot;]/div[@class=&quot;ant-form-item-control-input-content&quot;]/div[@class=&quot;ant-select ant-select-in-form-item ant-select-auto-complete ant-select-single ant-select-open ant-select-show-search&quot;]/div[@class=&quot;ant-select-selector&quot;]</value>
      <webElementGuid>4293f9d6-7d11-4576-b167-341a031b6a2b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='basic']/div[2]/div[2]/div/div/div/div</value>
      <webElementGuid>a3ad13cd-c922-421f-a90e-59726feaa7c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Item'])[1]/following::div[5]</value>
      <webElementGuid>2d07d252-cc19-4ac5-b5a6-77cb3a443d10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WELLNESS-TV VIDEO'])[2]/following::div[7]</value>
      <webElementGuid>e711185f-2449-4d72-bb5d-13beb3fea9ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/preceding::div[1]</value>
      <webElementGuid>ea92c33a-9cce-47ea-b5e5-90e515309d7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div</value>
      <webElementGuid>0dfc6591-eb75-4c66-9715-7a4bfcc4eac1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Search wellness tv' or . = 'Search wellness tv')]</value>
      <webElementGuid>3501e774-fb01-403c-8a79-099fbc189ff8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
