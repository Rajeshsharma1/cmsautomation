<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_WELLNESS-TV VIDEO</name>
   <tag></tag>
   <elementGuidId>b5d8e6f6-963c-48d5-8939-84d83f24b9c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='basic_category']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-select-item-option-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6ec64ad5-2c95-4fbf-9abd-d55e515c7b3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-item-option-content</value>
      <webElementGuid>53dc7ba0-6cc2-4bdc-ad4a-6405514d844f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>WELLNESS-TV VIDEO</value>
      <webElementGuid>d2cf727e-ea77-4a5d-a3c2-69ea5ae5defa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ant-scrolling-effect&quot;]/div[4]/div[1]/div[@class=&quot;ant-select-dropdown ant-select-dropdown-placement-bottomLeft&quot;]/div[1]/div[@class=&quot;rc-virtual-list&quot;]/div[@class=&quot;rc-virtual-list-holder&quot;]/div[1]/div[@class=&quot;rc-virtual-list-holder-inner&quot;]/div[@class=&quot;ant-select-item ant-select-item-option ant-select-item-option-active&quot;]/div[@class=&quot;ant-select-item-option-content&quot;]</value>
      <webElementGuid>edd2afe7-2625-4784-bd27-937d180eef06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='guided-programs'])[1]/following::div[6]</value>
      <webElementGuid>1e7f8c0a-5209-4c68-b208-31b67644fbd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='wellness-tv'])[1]/following::div[7]</value>
      <webElementGuid>078a6250-913e-4c3b-b91e-4aff124a35e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GUIDED PROGRAMS'])[1]/preceding::div[1]</value>
      <webElementGuid>60c47f18-a30c-465d-8622-1eaf8d393fac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RECIPE'])[1]/preceding::div[3]</value>
      <webElementGuid>5904a5c5-bc35-45dd-816b-b93586ef1001</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='WELLNESS-TV VIDEO']/parent::*</value>
      <webElementGuid>9a2686f6-8824-4b3d-966c-f7c99ce59f39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div</value>
      <webElementGuid>fd989a08-a740-428a-8864-0164b727e474</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'WELLNESS-TV VIDEO' or . = 'WELLNESS-TV VIDEO')]</value>
      <webElementGuid>cba6a97b-58ed-4991-8c3c-72819548976e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
