<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pawanmuktasana Part 2</name>
   <tag></tag>
   <elementGuidId>76df3e4c-ecf5-4d2b-af87-0f5ae1808ff7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Pawanmuktasana Part 2'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>42c58ab8-e882-4a4e-99f3-32b93f89a632</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0ff8ade8-b463-4319-bcf6-b4bf9c1a15fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-select-item ant-select-item-option ant-select-item-option-active</value>
      <webElementGuid>35b5709c-d086-4f91-8b36-c581c26d212e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pawanmuktasana Part 2</value>
      <webElementGuid>67598082-6835-4d3e-a0a7-36719b48acde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ant-scrolling-effect&quot;]/div[5]/div[1]/div[@class=&quot;ant-select-dropdown ant-select-dropdown-placement-bottomLeft&quot;]/div[1]/div[@class=&quot;rc-virtual-list&quot;]/div[@class=&quot;rc-virtual-list-holder&quot;]/div[1]/div[@class=&quot;rc-virtual-list-holder-inner&quot;]/div[@class=&quot;ant-select-item ant-select-item-option ant-select-item-option-active&quot;]</value>
      <webElementGuid>06512104-3653-4156-b9a7-c171dbd3c5d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It Takes One To Know One Understanding Stress, Anxiety &amp; Anger'])[1]/following::div[5]</value>
      <webElementGuid>5c9a84de-df0a-4801-b16f-a6c72da4411b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pawanmuktasana Part 2'])[1]/following::div[6]</value>
      <webElementGuid>49f031b6-ac35-44cd-8eb2-17f5beb90bf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It Takes One To Know One Understanding Stress, Anxiety &amp; Anger'])[2]/preceding::div[6]</value>
      <webElementGuid>e9b9d5af-0e6b-4839-9132-afb012107d4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div[2]/div/div/div/div</value>
      <webElementGuid>0d0b5b25-7eec-4144-b136-1d8996f1c4dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Pawanmuktasana Part 2' or . = 'Pawanmuktasana Part 2')]</value>
      <webElementGuid>a311a8a7-f93f-41ad-a553-baef8910af2a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
