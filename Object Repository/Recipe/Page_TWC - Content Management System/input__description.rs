<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__description</name>
   <tag></tag>
   <elementGuidId>e5ee29af-35b8-45c8-a6f0-eda2d4d3bfb4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;description&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='description']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9d1fd317-cc6b-4a6c-b66e-3c876dfe544c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>description</value>
      <webElementGuid>1d21c3e7-aa21-417b-8625-d3c44f0b7f87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter a short summary about the recipe</value>
      <webElementGuid>98430fda-6074-4892-95e0-cfe0e7495d95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>57fbc494-7e02-4e9a-a3ec-48e0ab58d49f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>5479aac4-bd4f-4b76-8e1d-7446d64c106a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Test</value>
      <webElementGuid>7ac57d9d-e1c8-4e9c-98b3-29b32a2fd254</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;session-modal width600 modal&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/form[1]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>580c8657-16c0-4adb-b55b-204685e63a39</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='description']</value>
      <webElementGuid>81bf1fde-9668-4c5f-8148-5644afc55c44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/input</value>
      <webElementGuid>5cd514d2-f52f-47a5-942c-d17b20d83b36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'description' and @placeholder = 'Enter a short summary about the recipe' and @type = 'text']</value>
      <webElementGuid>5386844f-1f4d-4d68-afb9-28f6ab9fb35a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
