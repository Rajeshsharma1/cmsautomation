<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add A Recipe</name>
   <tag></tag>
   <elementGuidId>c42b885b-89b3-4878-9cf8-c7eb18ae2f97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-info</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/section/section/div[2]/div/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>377a13ca-167a-4b62-9334-bbc01bd3ce2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-info</value>
      <webElementGuid>6c3779f1-d665-487e-ab07-fa29b6480808</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add A Recipe</value>
      <webElementGuid>97128a4b-8b2d-4958-a67e-c709589b1fe3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/section[@class=&quot;ant-layout ant-layout-has-sider&quot;]/section[@class=&quot;ant-layout&quot;]/div[2]/div[@class=&quot;recipeTable&quot;]/div[@class=&quot;table-container&quot;]/div[@class=&quot;toolbar&quot;]/button[@class=&quot;btn btn-info&quot;]</value>
      <webElementGuid>516ecdf6-6365-43c8-a89e-fcff05684c85</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/section/section/div[2]/div/div/div/button</value>
      <webElementGuid>dc2d30fb-4ef1-4d05-abf1-82b8e7de4c77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::button[1]</value>
      <webElementGuid>cf288ce7-ff2a-4890-ae2d-83b4291daf27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Activity Log'])[1]/following::button[2]</value>
      <webElementGuid>c0ee74e9-0148-47fc-a17b-c642bfd80a30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recipes'])[2]/preceding::button[1]</value>
      <webElementGuid>2ab9f849-51dd-4e39-b6c5-6a03deefe38a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recipes'])[3]/preceding::button[2]</value>
      <webElementGuid>b2a919a4-5e73-4b1e-9d3b-a99817e17834</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add A Recipe']/parent::*</value>
      <webElementGuid>c47fbaa6-a3d2-4561-a625-3a1eda91ce45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>36afccb3-661d-43de-893d-b8ce2c09f7bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add A Recipe' or . = 'Add A Recipe')]</value>
      <webElementGuid>adfea0bc-8aa4-432b-b1b1-5bc979d453ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
