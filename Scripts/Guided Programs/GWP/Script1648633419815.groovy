import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name2 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name3 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Click on Guided Wellness Program tab')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/a_Guided Programs'))

WebUI.comment('Click on Add Guided Program button')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/button_Add Guided Program'))

WebUI.comment('Enter the Name of GWP')

WebUI.setText(findTestObject('Guided Programs/Page_TWC - Content Management System/input__form-control'), 'testGuidedProgram' + 
    work_name.toString())

WebUI.comment('Click on Brouse to upload image')

WebUI.uploadFile(findTestObject('Guided Programs/Page_TWC - Content Management System/input__formcheck-api-custom'), GlobalVariable.imagepath)

WebUI.delay(2)

WebUI.comment('Enter Description ')

WebUI.setText(findTestObject('Guided Programs/Page_TWC - Content Management System/textarea__form-control'), 'testGuidedProgram' + 
    work_name1.toString())

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/button_Save'))

WebUI.delay(3)

WebUI.comment('Click on View icon')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/svg_medi'))

WebUI.delay(3)

WebUI.comment('Get Name of GWP')

name1 = WebUI.getText(findTestObject('Guided Programs/Page_TWC - Content Management System/gwp_text'))

name2 = ('testGuidedProgram' + work_name.toString())

WebUI.comment(name1)

WebUI.comment(name2)

if (name1 == name2) {
    WebUI.comment('matched')
} else {
    WebUI.comment('not matched')

    WebUI.closeBrowser()

    assert false : 'Guided program is not matched'
}

WebUI.comment('matched')

WebUI.delay(3)

WebUI.comment('Click on Edit Button')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/button_EDIT'))

WebUI.sendKeys(findTestObject('Guided Programs/Page_TWC - Content Management System/input__form-control'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.sendKeys(findTestObject('Guided Programs/Page_TWC - Content Management System/input__form-control'), Keys.chord(Keys.BACK_SPACE))

WebUI.comment('Edit the Name of GWP')

WebUI.setText(findTestObject('Guided Programs/Page_TWC - Content Management System/input__form-control'), 'testGuidedProgram' + 
    work_name2.toString())

WebUI.delay(4)

WebUI.uploadFile(findTestObject('Guided Programs/Page_TWC - Content Management System/Edit image'), GlobalVariable.imagepath2)

WebUI.comment('Upload image')

WebUI.comment('Enter Description')

WebUI.setText(findTestObject('Meditation/Page_TWC - Content Management System/textarea__description'), 'testGuidedProgram' + 
    work_name3.toString())

WebUI.comment('Cick on Update Button')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/button_Update'))

WebUI.delay(3)

WebUI.comment('Get text of GWP')

name3 = WebUI.getText(findTestObject('Guided Programs/Page_TWC - Content Management System/gwp_text'))

name4 = ('testGuidedProgram' + work_name2.toString())

WebUI.comment(name3)

WebUI.comment(name4)

if (name3 == name4) {
    WebUI.comment('matched')
} else {
    WebUI.comment('not matched')

    WebUI.closeBrowser()

    assert false : 'Guided program is not matched'
}

WebUI.comment('matched')

WebUI.comment('Click on Add Task Button')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/button_Add Task'))

WebUI.comment('Select the task from drop down')

WebUI.selectOptionByValue(findTestObject('Guided Programs/Page_TWC - Content Management System/select_task'), 'task', true)

WebUI.comment('Enter Task Title')

WebUI.setText(findTestObject('Guided Programs/Page_TWC - Content Management System/input__form-control'), 'testGuidedProgram' + 
    work_name2.toString())

WebUI.comment('Upload Image')

WebUI.uploadFile(findTestObject('Guided Programs/Page_TWC - Content Management System/input__formcheck-api-custom'), GlobalVariable.imagepath2)

WebUI.comment('Enter the Description')

WebUI.setText(findTestObject('Guided Programs/Page_TWC - Content Management System/textarea__form-control'), 'testGuidedProgram' + 
    work_name3.toString())

WebUI.comment('Click on Save Button')

WebUI.click(findTestObject('Guided Programs/Page_TWC - Content Management System/button_Save'))

WebUI.navigateToUrl(GlobalVariable.baseurl + 'guided-programs')

WebUI.comment('Click on Delete icon')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/svg_medi2'))

WebUI.comment('Click on Delete button')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/delete button'))

WebUI.delay(3)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

