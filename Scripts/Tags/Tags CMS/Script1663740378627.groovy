import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

CustomKeywords.'utilities.CMS_Customs.login'()

WebUI.comment('Click on Tag tab')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/li_Tags'))

WebUI.verifyElementText(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/span_Tags'), 
    'Tags')

WebUI.comment('Click on Create Tag button')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/button_Create Tag'))

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Enter the tag name')

WebUI.setText(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/input_Tag Name_basic_name'), 'Tag' + 
    work_name.toString())

WebUI.delay(3)

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/button_Save'))

Tag1 = WebUI.getText(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/td_Tag567'))

WebUI.comment(Tag1)

WebUI.comment('Click on View button')

WebUI.click(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/a'))

Tag2 = WebUI.getText(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/span_Tag329'))

WebUI.comment(Tag2)

if (Tag1 == Tag2) {
    WebUI.comment('Tag matched')
} else {
    WebUI.comment('Tag not matched')

    WebUI.closeBrowser()

    assert false : 'Tag name is not matched'
}

WebUI.comment('Click on Add Item button')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/button_Add Item'))

WebUI.delay(5)

WebUI.comment('Click on Dropdown to select category')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/div_WELLNESS-TV VIDEO'))

WebUI.delay(4)

WebUI.comment('Click on Wellness tv')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/div_Search wellness tv'))

WebUI.delay(5)

WebUI.comment('Click on Select item dropdown')

WebUI.click(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/input_Select Item_rc_select_5'))

WebUI.delay(3)

WebUI.comment('Get text of First item from dropdown')

Itemname1 = WebUI.getText(findTestObject('Tags/Page_TWC - Content Management System/div_Pawanmuktasana Part 2'))

WebUI.comment('Click on First item from dropdown')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/div_Pawanmuktasana Part 2'))

WebUI.comment('Click on save button')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/button_Save'))

Itemname2 = WebUI.getText(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/h6_Pawanmuktasana Part 2'))

if (Itemname1 == Itemname2) {
    WebUI.comment('Selected item matched')
} else {
    WebUI.comment('Selected item not matched')

    WebUI.closeBrowser()

    assert false : 'Item name is not matched'
}

WebUI.comment('Click on save button')

WebUI.click(findTestObject('Tags/Page_TWC - Content Management System/li_Tags'))

WebUI.delay(4)

WebUI.comment('Enter tag name in Search box')

WebUI.setText(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/span_Tags_ant-input-affix-wrapper ant-input-affix-wrapper-lg'), 
    Tag1)

WebUI.comment('click on search icon')

WebUI.click(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/button_Tags_ant-btn ant-btn-primary ant-btn-lg ant-input-search-button'))

WebUI.comment('Click on delete icon')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/svg'))

WebUI.delay(2)

WebUI.comment('Click on Yes button to delete ')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/button_Yes'))

WebUI.delay(3)

WebUI.comment('Tap on Cross icon to remove text from search field')

WebUI.click(findTestObject('Tags/Page_TWC - Content Management System/Page_TWC - Content Management System/span_Tags_ant-input-clear-icon'))

WebUI.comment('Tap on Logout button')

WebUI.click(findTestObject('Object Repository/Tags/Page_TWC - Content Management System/button_Logout'))

