import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name2 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Click on Assessment tab fromside menu')

WebUI.click(findTestObject('Assessments/a_Assessment'))

WebUI.comment('Click on Create assessment button')

WebUI.click(findTestObject('Assessments/button_create assessment'))

WebUI.comment('Enter Assessment name')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Assessment Name_basicForm_name'), 
    'test assessment')

WebUI.comment('Enter Description')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/textarea_test description'), 
    'test description')

WebUI.comment('Click on "Click to upload " to upload image')

WebUI.uploadFile(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/span_Click to Upload'), 
    GlobalVariable.imagepath)

WebUI.comment('Click on Select Type field')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/div_Select Type'))

WebUI.comment('Select Mindfullness')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/div_Mindfulness'))

WebUI.comment('Click on select color field')

//WebUI.click(findTestObject('Assessments/select color'))
WebUI.click(findTestObject('Assessments/select color'))

WebUI.delay(3)

WebUI.comment('Select color')

WebUI.mouseOverOffset(findTestObject('Assessments/select color'), 90, 80)

WebUI.delay(3)

WebUI.comment('Click on Save & Next button')

//WebUI.clickImage(findTestObject('Assessments/select color'))
WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/button_Save  Next'))

WebUI.comment('Enter the First Question')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Q1_questionsForm_questions_0_title'), 
    'test question for assessment?')

WebUI.comment('Enter Choice 1')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_A_questionsForm_questions_0_answers_0_title'), 
    'a')

WebUI.comment('Enter Choice 2')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_B_questionsForm_questions_0_answers_1_title'), 
    'b')

WebUI.comment('Enter the Choice 3')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_C_questionsForm_questions_0_answers_2_title'), 
    'c')

WebUI.comment('Enter the score for first otion')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_A_questionsForm_questions_0_answers_0_score'), 
    '2')

WebUI.comment('Enter the score for Second option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_B_questionsForm_questions_0_answers_1_score'), 
    '0')

WebUI.comment('Enter the score for Third option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_C_questionsForm_questions_0_answers_2_score'), 
    '3')

WebUI.comment('Enter the second Question')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Q2_questionsForm_questions_1_title'), 
    'test question for assessment2?')

WebUI.comment('Enter first option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_A_questionsForm_questions_1_answers_0_title'), 
    'x')

WebUI.comment('Enter second option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_B_questionsForm_questions_1_answers_1_title'), 
    'y')

WebUI.comment('Enter third option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_C_questionsForm_questions_1_answers_2_title'), 
    'z')

WebUI.comment('Enter the score for first otion')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_A_questionsForm_questions_1_answers_0_score'), 
    '2')

WebUI.comment('Enter the score for Second option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_B_questionsForm_questions_1_answers_1_score'), 
    '3')

WebUI.comment('Enter the score for Third option')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_C_questionsForm_questions_1_answers_2_score'), 
    '4')

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/button_Save  Next'))

WebUI.comment('Enter Text Result 1')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Result 1_resultsForm_score_0_title'), 
    'test result')

WebUI.comment('Enter Score Threshold')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Score Threshold_resultsForm_score_0_t_120c51'), 
    '5')

WebUI.comment('Enter the Description')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/textarea_test result description'), 
    'test result description')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Result 2_resultsForm_score_1_title'))

WebUI.comment('Enter Text for Result 2')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Result 2_resultsForm_score_1_title'), 
    'test result2')

WebUI.comment('Enter scre Threshold for second result')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/input_Score Threshold_resultsForm_score_1_t_5704ad'), 
    '10')

WebUI.comment('Enter Desciption For second result')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/textarea_test result description2'), 
    'test result description2')

WebUI.comment('')

WebUI.setText(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/div_test'), '<div data-contents="true"><div class="" data-block="true" data-editor="bv60p" data-offset-key="b6ki7-0-0"><div data-offset-key="b6ki7-0-0" class="public-DraftStyleDefault-block public-DraftStyleDefault-ltr" style=""><span data-offset-key="b6ki7-0-0"><span data-text="true">test</span></span></div></div></div>')

WebUI.comment('Click on Create Assessment')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/button_Create Assessment'))

WebUI.comment('Click on Delete icon')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/svg'))

WebUI.comment('Click on delete button')

WebUI.click(findTestObject('Object Repository/Assessments/Page_TWC - Content Management System/button_Delete'))

