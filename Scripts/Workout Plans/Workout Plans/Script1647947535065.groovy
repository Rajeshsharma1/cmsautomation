import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import groovy.ui.SystemOutputInterceptor as SystemOutputInterceptor

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name2 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Click on Create workout plan')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/button_Create Workout Plan'))

WebUI.comment('Enter the Plan name')

WebUI.setText(findTestObject('Object Repository/workout plans/Page_TWC - Content Management System/input__form-control'), 
    'testworkout' + work_name.toString())

WebUI.comment('Upload image')

WebUI.uploadFile(findTestObject('workout plans/Page_TWC - Content Management System/div_Plan Name A workout with this name exis_c5363b'), 
    GlobalVariable.imagepath)

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Object Repository/workout plans/Page_TWC - Content Management System/button_Save'))

WebUI.delay(3)

WebUI.comment('Click on View icon')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/svg1'))

WebUI.comment('Get the workout Name')

name1 = WebUI.getText(findTestObject('workout plans/Page_TWC - Content Management System/workout text'))

name2 = ('testworkout' + work_name.toString())

WebUI.comment(name1)

WebUI.comment(name2)

if (name1 == name2) {
    WebUI.comment('matched')
} else {
    WebUI.comment('not matched')

    WebUI.closeBrowser()

    assert false : 'workout name is not matched'
}

WebUI.delay(3)

name3 = ('testworkout' + work_name1.toString())

name4 = ('test workout description' + work_name2.toString())

WebUI.comment('Click on Add session Button')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/add session btn'))

WebUI.comment('Enter the session name')

WebUI.setText(findTestObject('workout plans/Page_TWC - Content Management System/input__form-control1'), 'testworkout' + 
    work_name1.toString())

WebUI.comment('Upload image')

WebUI.uploadFile(findTestObject('workout plans/Page_TWC - Content Management System/upload file'), GlobalVariable.imagepath)

WebUI.comment('Enter the Exercise included')

WebUI.setText(findTestObject('workout plans/Page_TWC - Content Management System/textarea_(Enter a short summary about the names of exercises in this workout session)_form-control'), 
    name4)

WebUI.comment('Choose the Youtube Radio button')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/input__formHorizontalRadios'))

WebUI.comment('Enter the URL of youtube video')

WebUI.setText(findTestObject('workout plans/Page_TWC - Content Management System/input__form-control2'), GlobalVariable.workouturl)

WebUI.comment('Click on save Button')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/button_Save'))

WebUI.delay(3)

WebUI.comment('Get workout name')

workname = WebUI.getText(findTestObject('workout plans/Page_TWC - Content Management System/workname'))

WebUI.comment('Get workout name')

workdetail = WebUI.getText(findTestObject('workout plans/Page_TWC - Content Management System/p_test'))

WebUI.comment(workname)

WebUI.comment(workdetail)

WebUI.comment(name3)

WebUI.comment(name4)

if (workname == name3) {
    WebUI.comment('matched')
} else {
    WebUI.comment('not matched')

    WebUI.closeBrowser()

    assert false : 'workout name is not matched'
}

if (workdetail == name4) {
    WebUI.comment('matched')
} else {
    WebUI.comment('not matched')

    WebUI.closeBrowser()

    assert false : 'workout name is not matched'
}

WebUI.delay(3)

WebUI.navigateToUrl(GlobalVariable.baseurl)

WebUI.comment('Click on Delete icon')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/svg2'))

WebUI.comment('Click on Delete button')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/delete button'))

WebUI.delay(5)

WebUI.closeBrowser()

