import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name2 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Click on Meditations Tab')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/a_Meditations'))

WebUI.comment('Click on Add Meditation Button')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/button_Create Meditation'))

WebUI.comment('Enter the Name')

WebUI.setText(findTestObject('Meditation/Page_TWC - Content Management System/input__name'), 'testMeditation' + work_name.toString())

WebUI.comment('Upload Meditation Image')

WebUI.uploadFile(findTestObject('workout plans/Page_TWC - Content Management System/upload file'), GlobalVariable.imagepath)

WebUI.comment('Enter the Description')

WebUI.setText(findTestObject('Meditation/Page_TWC - Content Management System/textarea__description'), 'testMeditation' + 
    work_name1.toString())

WebUI.comment('Select Radio button for video source')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/input__formHorizontalRadios'))

WebUI.comment('Enter video URL')

WebUI.setText(findTestObject('Meditation/Page_TWC - Content Management System/input__url'), GlobalVariable.workouturl)

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/button_Save'))

WebUI.delay(3)

WebUI.comment('Click on View icon')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/svg_medi'))

WebUI.delay(3)

WebUI.comment('Click on Cross icon')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/close popup'))

WebUI.comment('Click on Edit icon')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/svg_medi2'))

WebUI.comment('Edit the name')

WebUI.setText(findTestObject('Meditation/Page_TWC - Content Management System/input__name'), 'testMeditation' + work_name.toString())

WebUI.comment('Upload edited image')

WebUI.uploadFile(findTestObject('workout plans/Page_TWC - Content Management System/upload file'), GlobalVariable.imagepath)

WebUI.comment('Enter Edited Description')

WebUI.setText(findTestObject('Meditation/Page_TWC - Content Management System/medi_description'), 'testMeditation' + work_name1.toString())

WebUI.comment('Click on Save Button')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/button_Save'))

WebUI.delay(3)

WebUI.comment('Click on Delete icon')

WebUI.click(findTestObject('Meditation/Page_TWC - Content Management System/svg_medi3'))

WebUI.comment('Click on Delete Button')

WebUI.click(findTestObject('workout plans/Page_TWC - Content Management System/delete button'))

WebUI.delay(3)

