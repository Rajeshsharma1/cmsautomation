import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

WebUI.comment('Click on Food tab')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/i_Food_ant-menu-submenu-arrow'))

WebUI.comment('Click on Food Items')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/li_Food Items'))

WebUI.verifyElementText(findTestObject('Food items/Page_TWC - Content Management System/span_Food Items'), 'Food Items')

WebUI.verifyElementText(findTestObject('Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/a_Create A Food Item'), 
    'Create A Food Item')

WebUI.comment('Click on Create a Food Item button')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/a_Create A Food Item'))

WebUI.verifyElementText(findTestObject('Food items/Page_TWC - Content Management System/span_Create Food'), 'Create Food')

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Enter the Food Name')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Food Name_basicForm_name'), 
    'Food' + work_name.toString())

WebUI.comment('Enter the Cuisine')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Cuisine_basicForm_cuisine'), 
    'Indian')

WebUI.comment('Click on Food Type dropdown')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/input_Food Type_basicForm_foodType'))

WebUI.comment('Select Vegetarian')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_Vegetarian'))

WebUI.comment('Click on Upload file button')

WebUI.uploadFile(findTestObject('Tags/upload file1'), GlobalVariable.imagepath)

WebUI.comment('Click on alias field')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/div_ (1)'))

WebUI.comment('Enetr the Alias')

WebUI.setText(findTestObject('Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/input_Alias_basicForm_alias'), 
    'test')

WebUI.comment('Select test')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/body_You need to enable JavaScript to run t_8351bc'))

WebUI.comment('Click on Tag field')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_'))

WebUI.comment('Enetr the Tag')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Tags_basicForm_hashtags'), 
    'Food1')

WebUI.comment('Select Tag')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_Food1'))

WebUI.comment('Enter the serving size')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/input_Serving Size_basicForm_servingSizes_0_servingSize'))

WebUI.comment('Select serving size')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_Soup Bowl'))

WebUI.comment('Enter the standard measurment')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input__basicForm_servingSizes_0_standardMea_3aa106'), 
    '10')

WebUI.comment('Click on Standard dropdown')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/input__basicForm_servingSizes_0_standardMeasure_unit'))

WebUI.comment('Select Gram in standard')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_grams'))

WebUI.comment('Enter Calories')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Calories (in kcal)_basicForm_servingS_36f944'), 
    '20')

WebUI.comment('Enter Fat')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Fat (in gms)_basicForm_servingSizes_0_fat'), 
    '25')

WebUI.comment('Enter Carbohydrate')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Carbohydrate (in gms)_basicForm_servi_500ed5'), 
    '30')

WebUI.comment('Enter protein')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Protein (in gms)_basicForm_servingSiz_0c42e3'), 
    '35')

WebUI.comment('Enter sodium')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Sodium (in mg)_basicForm_servingSizes_3f8099'), 
    '40')

WebUI.comment('Enter fibre')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Fiber (in gms)_basicForm_servingSizes_7ff95d'), 
    '45')

WebUI.comment('Switch on the Is Recommended button')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/button_Is Recommended_basicForm_servingSize_53cfde'))

WebUI.comment('Click on Add Serving Size button')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/Page_TWC - Content Management System/button_Add Serving Size'))

WebUI.comment('Click on Serving size field')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/input_Serving Size_basicForm_servingSizes_1_servingSize'))

WebUI.comment('Select serving size')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_Katori'))

WebUI.comment('Enter standard Measurement')

WebUI.setText(findTestObject('Food items/Page_TWC - Content Management System/input__basicForm_servingSizes_1_standardMeasure_quantity'), 
    '30')

WebUI.comment('Click on Standard dropdown')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/input__basicForm_servingSizes_1_standardMeasure_unit'))

WebUI.comment('Select ml')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/div_ml'))

WebUI.comment('Eneter Calories')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Calories (in kcal)_basicForm_servingS_e54922'), 
    '5')

WebUI.comment('Enter Fat')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Fat (in gms)_basicForm_servingSizes_1_fat'), 
    '10')

WebUI.comment('Enter Carbohydrate')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Carbohydrate (in gms)_basicForm_servi_f33063'), 
    '10')

WebUI.comment('Enter Protein')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Protein (in gms)_basicForm_servingSiz_df9942'), 
    '10')

WebUI.comment('Enter Sodium')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Sodium (in mg)_basicForm_servingSizes_7ac3d5'), 
    '10')

WebUI.comment('Enter fibre')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Fiber (in gms)_basicForm_servingSizes_feb785'), 
    '10')

WebUI.comment('Click on Save Food Button')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/button_Save Food'))

WebUI.delay(4)

WebUI.comment('Get the Food Name')

Foodname1 = WebUI.getText(findTestObject('Food items/Page_TWC - Content Management System/td_1234'))

WebUI.comment('Get the Food type')

Foodtype1 = WebUI.getText(findTestObject('Food items/Page_TWC - Content Management System/td_Vegetarian'))

WebUI.comment('Click on View icon')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/svg_1'))

WebUI.comment('Capture the food name')

Foodname2 = WebUI.getText(findTestObject('Food items/Page_TWC - Content Management System/h4_1234'))

WebUI.comment('Capture the food type')

Foodtype2 = WebUI.getText(findTestObject('Food items/Page_TWC - Content Management System/strong_Vegetarian'))

if (Foodname1 == Foodname2) {
    WebUI.comment('Food name matched')
} else {
    WebUI.comment('fodd name not matched')

    WebUI.closeBrowser()

    assert false : 'Food name is not matched'
}

if (Foodtype1 == Foodtype2) {
    WebUI.comment('Food type matched')
} else {
    WebUI.comment('Food type not matched')

    WebUI.closeBrowser()

    assert false : 'Fodd type is not matched'
}

WebUI.comment('Click on Cross icon')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/span__ant-modal-close-x'))

WebUI.comment('Click on Edit icon')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/svg_1_2'))

WebUI.sendKeys(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Food Name_basicForm_name'), 
    Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Food Name_basicForm_name'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.comment('Edit the Food Item Name')

WebUI.setText(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/input_Food Name_basicForm_name'), 
    '1234')

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/button_Save Food'))

WebUI.delay(4)

Foodname3 = WebUI.getText(findTestObject('Food items/Page_TWC - Content Management System/td_1234'))

if (Foodname3 != Foodname1) {
    WebUI.comment('Food name edited sucessfully')
} else {
    WebUI.comment('Food name not edited')

    WebUI.closeBrowser()

    assert false : 'Food name not edited'
}

WebUI.delay(3)

WebUI.comment('Click on Delete icon of first Food item')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/svg_1_2_3'))

WebUI.comment('Click on Yes button')

WebUI.click(findTestObject('Object Repository/Food items/Page_TWC - Content Management System/button_Yes'))

WebUI.delay(4)

WebUI.comment('Click on Logout button')

WebUI.click(findTestObject('Food items/Page_TWC - Content Management System/button_Logout'))

WebUI.closeBrowser()

