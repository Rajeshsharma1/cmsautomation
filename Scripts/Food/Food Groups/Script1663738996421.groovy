import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

WebUI.comment('click on food tab')

WebUI.click(findTestObject('Food Group/Page_TWC - Content Management System/div_Food'))

WebUI.comment('Click on Food Group option')

WebUI.click(findTestObject('Food Group/Page_TWC - Content Management System/li_Food Groups'))

WebUI.comment('User click on Create Food Group button')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Create Food Group'))

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Enter the Food Group name')

WebUI.setText(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Food Group Name_basic_name'), 
    'Food' + work_name.toString())

WebUI.comment('Click on meal type dropdown')

WebUI.click(findTestObject('Food Group/Page_TWC - Content Management System/input_Meal Type_basic_mealType'))

WebUI.comment('Select Breakfast from the dropdown')

WebUI.click(findTestObject('Food Group/Page_TWC - Content Management System/Breakfast select'))

WebUI.delay(3)

WebUI.comment('Click on Save button to save Food group')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Save'))

WebUI.delay(5)

WebUI.comment('Capture the food group name')

Foodname1 = WebUI.getText(findTestObject('Food Group/Page_TWC - Content Management System/td_Food371'))

WebUI.comment(Foodname1)

Foodtype1 = WebUI.getText(findTestObject('Food Group/Page_TWC - Content Management System/td_Breakfast'))

WebUI.comment(Foodtype1)

WebUI.delay(2)

WebUI.comment('Click on Edit button of created food group')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/a'))

WebUI.comment('Capture the food group name from the description page')

Foodname2 = WebUI.getText(findTestObject('Food Group/Page_TWC - Content Management System/span_Food371 (Breakfast)'))

WebUI.comment(Foodname2)

Foodname4 = (((Foodname1 + ' (') + Foodtype1) + ')')

WebUI.comment(Foodname4)

if (Foodname2 == Foodname4) {
    WebUI.comment('Food name and type matched')
} else {
    WebUI.comment('Food name and type not matched')

    WebUI.closeBrowser()

    assert false : 'Food name and type not matched'
}

WebUI.comment('Click on Add food button')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Add Food'))

WebUI.comment('Click on Search field')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/div_Search Food'))

WebUI.comment('Search Milk')

WebUI.setText(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Enter Food Name_basic_foodId'), 
    'milk')

WebUI.comment('Select Soya milk')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/div_Soy Milk'))

WebUI.comment('Click on Save button to save food')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Save'))

WebUI.comment('Again click on Add Food button')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Add Food'))

WebUI.comment('Click on Search Food Field')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/div_Search Food'))

WebUI.comment('Search Juice')

WebUI.setText(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Enter Food Name_basic_foodId'), 
    'juice')

WebUI.comment('Select Sugarcane juice')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/div_Sugarcane, juice (Ganne Ka Ras)'))

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Save'))

WebUI.comment('Click on back icon')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/svg'))

WebUI.comment('Click on Edit icon')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/svg_1'))

WebUI.sendKeys(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Food Group Name_basic_name'), 
    Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Food Group Name_basic_name'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.comment('Edit food Group name')

WebUI.setText(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Food Group Name_basic_name'), 
    'Food' + work_name1.toString())

WebUI.comment('Clickon Save button')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/button_Save'))

WebUI.comment('Capture the Edited Food group name text')

Foodname3 = WebUI.getText(findTestObject('Food Group/Page_TWC - Content Management System/td_Food722'))

WebUI.comment(Foodname3)

if (Foodname3 != Foodname1) {
    WebUI.comment('Food nameEdited successfully')
} else {
    WebUI.comment('fodd name not Edited')

    WebUI.closeBrowser()

    assert false : 'Food name is not Edited'
}

WebUI.comment('Click on Edit button')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/a'))

WebUI.comment('Click on delete icon of first food in the list')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/svg_1_2'))

WebUI.mouseOver(findTestObject('login/Page_TWC - Content Management System/button_Logout'))

WebUI.delay(2)

WebUI.comment('Click on Yes button')

WebUI.click(findTestObject('Food Group/Page_TWC - Content Management System/Delete icon tab'))

WebUI.comment('Click on Back icon')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/svg'))

WebUI.comment('Enter the Edited food group name in search box')

WebUI.setText(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/input_Food Groups_ant-input ant-input-lg'), 
    Foodname3)

WebUI.comment('Click on search icon')

WebUI.click(findTestObject('Object Repository/Food Group/Page_TWC - Content Management System/svg_1_2_3'))

WebUI.delay(3)

WebUI.comment('Click on delete icon')

WebUI.click(findTestObject('Food Group/Page_TWC - Content Management System/Delete icon tab'))

WebUI.delay(4)

WebUI.closeBrowser()

