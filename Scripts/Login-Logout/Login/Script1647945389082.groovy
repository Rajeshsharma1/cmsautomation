import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.comment('Navigate to the CMS web URL')

WebUI.navigateToUrl(GlobalVariable.baseurl)

WebUI.comment('Enter Username')

WebUI.setText(findTestObject('login/Page_TWC - Content Management System/input_Email address_formBasicEmail'), GlobalVariable.username)

WebUI.comment('Enter Password')

WebUI.setText(findTestObject('login/Page_TWC - Content Management System/input_Password_formBasicPassword'), GlobalVariable.password)

WebUI.comment('Click on Login button')

WebUI.click(findTestObject('login/Page_TWC - Content Management System/button_Submit'))

if (WebUI.verifyElementPresent(findTestObject('login/Page_TWC - Content Management System/div_Invalid password'), 10, FailureHandling.OPTIONAL)) {
    WebUI.comment('User is not logged in due to invalid credentials.')

    WebUI.closeBrowser()

    assert false : 'User is not logged in due to invalid credentials.'
}

if (WebUI.verifyElementPresent(findTestObject('login/Page_TWC - Content Management System/button_Logout'), 10)) {
    WebUI.click(findTestObject('login/Page_TWC - Content Management System/button_Logout'))

    WebUI.comment('User logged in successfully.')
} else {
    WebUI.comment('User not logged in.')

    WebUI.closeBrowser()

    assert false : 'Test case failed- User not logged in successfully.'
}

WebUI.closeBrowser()

