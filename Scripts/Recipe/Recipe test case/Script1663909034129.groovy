import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

CustomKeywords.'utilities.CMS_Customs.login'()

WebUI.comment('Click on Recipe tab')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/li_Recipes'))

WebUI.verifyElementText(findTestObject('Recipe/Page_TWC - Content Management System/div_Recipes'), 'Recipes')

WebUI.comment('Click on add a Recipe button')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/button_Add A Recipe'))

String candidateChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

StringBuilder degree1 = new StringBuilder()

Random random = new Random()

for (int i = 0; i < 6; i++) {
    degree1.append(candidateChars.charAt(random.nextInt(candidateChars.length())))
}

def work_name = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name1 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name2 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

def work_name3 = (((Math.random() * ((1000 - 100) + 1)) + 1) as int)

WebUI.comment('Enter the Recipe name')

WebUI.setText(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__name'), 'Recipe' + work_name.toString())

WebUI.comment('Upload recipe image')

WebUI.uploadFile(findTestObject('Recipe/Page_TWC - Content Management System/input__formcheck-api-custom1'), GlobalVariable.imagepath)

WebUI.comment('Enter the Description')

WebUI.setText(findTestObject('Recipe/Page_TWC - Content Management System/Description text box'), 'Description' + work_name2.toString())

WebUI.comment('Click on youtube radio button')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__formHorizontalRadios'))

WebUI.comment('Enter the Youtube link')

WebUI.setText(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__url'), 'https://www.youtube.com/watch?v=1piFN_ioMVI')

WebUI.comment('Click on Save button')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/button_Save'))

WebUI.delay(3)

WebUI.comment('Capture the recipe name')

Recipename1 = WebUI.getText(findTestObject('Recipe/Page_TWC - Content Management System/capture recipe1'))

WebUI.comment(Recipename1)

WebUI.comment('Click on View icon of first recipe from the list')

WebUI.click(findTestObject('Recipe/Page_TWC - Content Management System/Click on View icon'))

WebUI.delay(3)

WebUI.comment('Capture the recipe name')

Recipename2 = WebUI.getText(findTestObject('Recipe/Page_TWC - Content Management System/td_Food287'))

WebUI.comment(Recipename2)

if (Recipename1 == Recipename2) {
    WebUI.comment('Recipe name matched')
} else {
    WebUI.comment('Recipe name not matched')

    WebUI.closeBrowser()

    assert false : 'Recipe name is not matched'
}

Description1 = WebUI.getText(findTestObject('Recipe/Page_TWC - Content Management System/p_Test'))

WebUI.comment(Description1)

WebUI.comment('Click on Close icon')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/button_Close'))

WebUI.comment('Click on Edit icon of first recipe from the list')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/svg_qwerty_MuiSvgIcon-root'))

WebUI.sendKeys(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__name'), Keys.chord(Keys.CONTROL, 
        'a'))

WebUI.sendKeys(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__name'), Keys.chord(Keys.BACK_SPACE))

WebUI.comment('Edit the Recipe Name')

WebUI.setText(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__name'), 'Recipe' + work_name1.toString())

WebUI.sendKeys(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__description'), Keys.chord(
        Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__description'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.comment('Edit the Description')

WebUI.setText(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/input__description'), 'Description' + 
    work_name3.toString())

WebUI.comment('Click on update button')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/button_Update'))

WebUI.delay(3)

WebUI.comment('Click on View icon of first Recipe from the list')

WebUI.click(findTestObject('Recipe/Page_TWC - Content Management System/Click on View icon'))

WebUI.delay(3)

WebUI.comment('Get the Recipe name')

Recipename3 = WebUI.getText(findTestObject('Recipe/Page_TWC - Content Management System/td_Food287'))

WebUI.comment(Recipename3)

if (Recipename3 != Recipename2) {
    WebUI.comment('Recipe name Edited successfully')
} else {
    WebUI.comment('Recipe name not Edited')

    WebUI.closeBrowser()

    assert false : 'Recipe name is not Edited'
}

Description2 = WebUI.getText(findTestObject('Recipe/Page_TWC - Content Management System/p_Test'))

WebUI.comment(Description2)

if (Description1 != Description2) {
    WebUI.comment('Description changed successfully')
} else {
    WebUI.comment('Description not changed successfully')

    WebUI.closeBrowser()

    assert false : 'Description notchanged successfully'
}

WebUI.comment('Click on Close icon')

WebUI.click(findTestObject('Recipe/Page_TWC - Content Management System/button_Close'))

WebUI.comment('Click on Delete icon')

WebUI.click(findTestObject('Recipe/Page_TWC - Content Management System/Delete icon'))

WebUI.delay(2)

WebUI.comment('Click on Delete button')

WebUI.click(findTestObject('Object Repository/Recipe/Page_TWC - Content Management System/button_Delete'))

WebUI.closeBrowser()

